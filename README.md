# Youtube #

[![Build Status](https://travis-ci.org/dhedegaard/youtube.svg?branch=master)](https://travis-ci.org/dhedegaard/youtube)
[![Coverage Status](https://coveralls.io/repos/dhedegaard/youtube/badge.svg?branch=master)](https://coveralls.io/r/dhedegaard/youtube?branch=master)
[![Requirements Status](https://requires.io/github/dhedegaard/youtube/requirements.svg?branch=master)](https://requires.io/github/dhedegaard/youtube/requirements/?branch=master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/605a167cdcf04637be5ef0b97d1bc1f5)](https://www.codacy.com/app/dhedegaard/youtube?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=dhedegaard/youtube&amp;utm_campaign=Badge_Grade)

A simple Django application for aggregating youtube channels.
